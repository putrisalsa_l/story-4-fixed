from django import forms
from homepage import models

class DateInput(forms.DateInput):
	input_type = 'date'

class EventForm(forms.Form):
	title = forms.CharField(
			label = 'Title :',
			max_length = 80,
			required = True,
			widget = forms.TextInput(attrs={
									'class' : 'form-control',
									'type' : 'text',
									'placeholder' : "What's up?"})
	)
	date = forms.DateField(
			label = 'Date :',
			required = True,
			widget = forms.DateInput(attrs={
									'class' : 'form-control',
									'type' : 'date'})
	)
	time = forms.TimeField(
        label = 'Time',
        input_formats = ['%H:%M'],
        widget = forms.TextInput(attrs={'class' : 'form-control form-control-sm',
        								'placeholder' : '00:00'})
    )
	category = forms.ChoiceField(
			label = 'Category :',
			required = True,
			choices = [('goal', 'Goal'),
					  ('memo', 'Memo'),
					  ('schedule', 'Schedule'),
					  ('meeting', 'Meeting'),
					  ('hangout', 'Hangout'),
					  ('other', 'Other')],
			widget = forms.Select(attrs={'class' : 'form-control form-control-sm'})
	)
	notes = forms.CharField(
			label = 'Notes :',
			required = False,
			widget = forms.Textarea(attrs={
									'class' : 'form-control',
									'type' : 'text',
									'placeholder' : "Here for some notes / Memo"})
	)

class Meta:
	model = models.savedEvent
	fields = ('title', 'date', 'time', 'category', 'notes')