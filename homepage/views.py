from django.shortcuts import render, redirect
from django.views.generic.edit import DeleteView
from django.urls import reverse_lazy
from homepage import forms, models
from .models import savedEvent
from datetime import datetime

class eventDelete(DeleteView):
	model = savedEvent
	success_url = reverse_lazy('homepage:event')

def home(request):
	return render(request, 'Home.html')

def about(request):
	return render(request, 'About.html')

def contact(request):
	return render(request, 'Contact.html')

def event(request):
	if request.method == 'POST':
		print("a")
		form = forms.EventForm(request.POST)

		if 'id' in request.POST:
			savedEvent.objects.get(id=request.POST['id']).delete()
			return redirect('/event/')

		if form.is_valid():
			print("b")
			event = models.savedEvent(
					title = form.data['title'],
					date = form.data['date'],
					time = form.data['time'],
					category = form.data['category'],
					notes = form.data['notes']
			)
			event.save()
			return redirect('/event/')

	else:
		form = forms.EventForm()

	event_content = {	'events_active' : 'active',
						'context' : {'now' : datetime.now()},
						'events' : models.savedEvent.objects.all().values(),
						'form' : form   }
	return render(request, 'event.html', event_content)