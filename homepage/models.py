from django.db import models

class savedEvent(models.Model):
	title = models.CharField(max_length=80)
	date = models.DateField()
	time = models.TimeField()
	category = models.CharField(max_length=50)
	notes = models.CharField(max_length=300)