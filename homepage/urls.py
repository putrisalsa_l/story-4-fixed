from django.urls import path
from django.conf.urls import url
from . import views

app_name='homepage'

urlpatterns = [
	path('', views.home, name='home'),
	path('About/', views.about, name='about'),
	# path('Event/', views.event, name='event'),
	path('Contact/', views.contact, name='contact'),
	path('event/', views.event, name='event'),
	url(r'event/(?P<pk>[0-9]+)/delete/$', views.eventDelete.as_view(), name='eventDelete')
]